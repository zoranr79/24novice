<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    
	protected $fillable = array('userId', 'article_id', 'title', 'image', 'section', 'published_from');
	
	protected $table = 'articles';
	
	
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use OAuth\OAuth2\Service\Google;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;


class OauthUserController extends Controller
{
    
	public function user(Request $request) 
	{
		$user = $request->session()->get('oauth-user');
		if (is_null($user))
		{
			$googleService = \OAuth::consumer('Google', url('oauth'));
			$url = $googleService->getAuthorizationUri();

			return response()->json(["logedin" => false, "url" => (string)$url]);
		}
		else
		{
			return response()->json(["logedin" => true]);	
		}
	}
	
	public function oauth(Request $request)
	{
		$code = $request->get('code');
		if (is_null($code) || $code == "")
		{
			return response()->json(["logedin" => false]);		
		}
		
		$googleService = \OAuth::consumer('Google', url('oauth'));
	
		$token = $googleService->requestAccessToken($code);
	
		$result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
		
		$request->session()->put('oauth-user', (object)$result);
		$valid = true;
		return view("oauthCallback", compact('valid'));
		//return response()->json(["logedin" => true]);
	}
	
}

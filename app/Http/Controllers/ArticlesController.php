<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Articles;


class ArticlesController extends Controller
{
    
	public function index(Request $request)
	{	
		$user = $request->session()->get('oauth-user');
		if (is_null($user))
		{
			return response()->json(['status' => 'fail', 'reason' => 'user', 'message' => 'No user', 'articles' => null]);
		}
		
		$articles = Articles::where('visible', '=', true)->where('userId', '=', $user->id)->orderBy('published_from', 'desc')->take(20)->get();
		
		return response()->json(['status' => 'ok', 'reason' => '', 'message' => 'Article exists', 'articles' => $articles]);
	}
	
	public function set(Request $request)
	{
		$user = $request->session()->get('oauth-user');
		if (is_null($user))
		{
			return response()->json(['status' => 'fail', 'reason' => 'user', 'message' => 'No user', 'articles' => null]);
		}
		
		if (!$request->has('articleData'))
		{
			return response()->json(['status' => 'fail', 'reason' => '', 'added' => 'false', 'message' => 'No POST data']);	
		}
		
		$articleData = (object)$request->input("articleData");
		if (is_null($articleData))
		{
			return response()->json(['status' => 'fail', 'reason' => '', 'added' => 'false', 'message' => 'No data']);
		}
		
		$article = Articles::where('article_id', '=', $articleData->id)->where('userId', '=', $user->id)->get();
		if (!$article->isEmpty())
		{
			return response()->json(['status' => 'ok', 'reason' => '', 'added' => 'false', 'message' => 'Article exists']);
		}

		$article = new Articles();
		$article->userId = $user->id;
		$article->article_id = $articleData->id;
		$article->title = $articleData->title;
		$article->image = $articleData->image;
		$article->section_name = $articleData->section_name;
		$article->published_from = $articleData->published_from;
		$article->save();
		
		return response()->json(['status' => 'ok', 'reason' => '', 'added' => 'true', 'message' => 'Success']);
	}
	
	public function delete(Request $request)
	{
		$user = $request->session()->get('oauth-user');
		if (is_null($user))
		{
			return response()->json(['status' => 'fail', 'reason' => 'user', 'message' => 'No user', 'articles' => null]);
		}
		
		if (!$request->has('article_id'))
		{
			return response()->json(['status' => 'fail', 'reason' => '', 'added' => 'false', 'message' => 'No POST data']);			
		}
		
		$articleId = $request->input("article_id");
		
		$article = Articles::where('id', '=', $articleId)->where('userId', '=', $user->id)->first();
		if (is_null($article))
		{
			return response()->json(['status' => 'ok', 'reason' => '', 'removed' => 'false', 'message' => 'No article']);
		}
		
		$article->visible = 0;
		$article->save();
		
		$articles = Articles::where('visible', '=', true)->where('userId', '=', $user->id)->take(20)->get();
		
		return response()->json(['status' => 'ok', 'reason' => '', 'removed' => 'true', 'message' => 'Article removed', 'articles' => $articles]);
	}
}

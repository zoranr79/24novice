<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('articles', "ArticlesController@index");

Route::post('articles', "ArticlesController@set");

Route::delete('articles', "ArticlesController@delete");

Route::get('user', "OauthUserController@user");
Route::get('oauth', "OauthUserController@oauth");


'use strict';

describe('Service: favourites', function () {

  // load the service's module
  beforeEach(module('articlesApp'));

  // instantiate service
  var favourites;
  beforeEach(inject(function (_favourites_) {
    favourites = _favourites_;
  }));

  it('should do something', function () {
    expect(!!favourites).toBe(true);
  });

});

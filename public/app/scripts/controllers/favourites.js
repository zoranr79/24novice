'use strict';

/**
 * @ngdoc function
 * @name articlesApp.controller:FavouritesCtrl
 * @description
 * # FavouritesCtrl
 * Controller of the articlesApp
 */
angular.module('articlesApp').controller('FavouritesCtrl', function ($scope, $window, favouritesService, Notification, helpers) {
  	var vm = this;
  
  	vm.login = false;
  	vm.articles = null;
  	
  	loadFavourites();
	
	function loadFavourites() {
		favouritesService.getFavouritesData().then(
			function(data) {
				if (data.status == "ok") vm.articles = data.articles;
				else if (data.status == "fail" && data.reason == "user")
				{
					vm.login = true;
				}
			}
		);
	}
	
	vm.loginUser = function()
	{
		helpers.userStatus().then(
			function(response) {
				if (response)
				{
					if (response.logedin) loadFavourites();
					else {	
						$window.loginResponse = function(valid) {
							popupWindow.close();
				            if (valid) 
				            {
				            	loadFavourites();
				            }
				        } 
						var popupWindow = window.open(response.url);
					}		
				}
				else
				{
					alert("Problem");
				}
			});
	}
	
	vm.removeFromFavorites = function(articleId)
	{
		favouritesService.removeFromFavourites(articleId).then(
				function(response) {
					if (response) {
						if (response.status == "ok" && response.removed == "true") 
						{	
							Notification.info(response.message);
							
							vm.articles = response.articles;
						}
						else if (response.status == "ok" && response.removed == "false") Notification.warning(response.message);
						else Notification.error(response.message);
					}
					else {
						Notification.error("Problem :)");
					}
				}
			);
	}
		
});

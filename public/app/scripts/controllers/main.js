'use strict';

/**
 * @ngdoc function
 * @name articlesApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the articlesApp
 */
angular.module('articlesApp').controller('MainCtrl', function ($scope) {

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name articlesApp.controller:ArticlesCtrl
 * @description
 * # ArticlesCtrl
 * Controller of the articlesApp
 */

angular.module('articlesApp').controller('ArticlesCtrl', function ($scope, $window, articlesService, helpers, Notification) {
	
	var vm = this;
	

	loadRemoteArticles();
	
	function loadRemoteArticles() {
		articlesService.getCurrentData().then(
			function(articlesData) {
				vm.articles = articlesData.data.list;
			}
		);
	}
	
	vm.addToFavorites = function(articleId)
	{
		helpers.userStatus().then(
			function(response) {
				if (response)
				{
					if (response.logedin) addToFavorites(articleId);
					else
					{
						$window.loginResponse = function(valid) {
							 popupWindow.close();
				            if (valid) 
				            {
				            	addToFavorites(articleId);
				            }
				        } 
						var popupWindow = window.open(response.url);
					}	
				}
			});
	}
	
	function addToFavorites(articleId)
	{
		angular.forEach(vm.articles, function(article, key) {
			if (article.id == articleId)
			{
				helpers.addToFavorites(article).then(
					function(response) {
						if (response) {
							if (response.status == "ok" && response.added == "true") Notification.info(response.message);
							else if (response.status == "ok" && response.added == "false") Notification.warning(response.message);
							else Notification.error(response.message);
						}
						else {
							Notification.error("Problem :)");
						}
					});
			}
		});
		
	}
	
}).filter('imageSize', function() {
	return function(imageUrl) {
		if (imageUrl) return imageUrl.replace("##WIDTH##", "237").replace("##HEIGHT##", "237");
		else "";
	}
}).filter('section', function() {
	return function(section) {
		var sections = section.split("/");
		return sections[sections.length - 1];		
	}
});

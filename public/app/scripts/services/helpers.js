'use strict';

/**
 * @ngdoc service
 * @name articlesApp.helpers
 * @description
 * # helpers
 * Factory in the articlesApp.
 */
angular.module('articlesApp').factory('helpers', ['$http', '$q', function ($http, $q) {
    
	var helpers = {};

    var articlesApiUrl = "http://laravel-articles.rudic.net/articles";
    var userUrl = "http://laravel-articles.rudic.net/user";
    
    // Public API here
    
    helpers.addToFavorites = function (articleData) {
    	var request = $http({
			method: "post",
			url: articlesApiUrl,
			params: {},
			data: {articleData: articleData}
		});
		return(request.then(handleSuccess, handleError));
	}
    
    helpers.getFavorites = function () {
    	var request = $http({
			method: "get",
			url: articlesApiUrl,
			params: {limit : 20}
		});
		return(request.then(handleSuccess, handleError));
	}
    
    helpers.removeFromFavourites = function (articleId) {
    	var request = $http({
			method: "delete",
			url: articlesApiUrl,
			params: {article_id : articleId}
		});
		return(request.then(handleSuccess, handleError));
	}
    
    helpers.userStatus = function () {
    	var request = $http({
			method: "get",
			url: userUrl,
			params: {}
		});
		return(request.then(handleSuccess, handleError));
	}
    
    function handleError(response) {

		if (response.status == "403")
		{
			//AuthenticationService.ClearCredentials();
			//$location.path('/login');
			//return;
		}
		
		if (!angular.isObject( response.data ) || !response.data.message) 
		{
			return( $q.reject( "An unknown error occurred." ) );
		}
		// Otherwise, use expected error message.
		return( $q.reject(response.data.message) );
	}
    
    function handleSuccess(response) 
	{
		return (response.data);
	}
    
    return helpers;

  }]);

'use strict';

/**
 * @ngdoc service
 * @name articlesApp.articles
 * @description
 * # articles
 * Service in the articlesApp.
 */
/*, 'AuthenticationService', 'FlashService'   , AuthenticationService, FlashService*/
angular.module('articlesApp').service('articlesService', ['$http', '$q', '$location', function ($http, $q, $location) {
	
	var service = {};
	
	service.getCurrentData = getCurrentData;

	return service;
  	
  
	function getCurrentData() 
 	{
		var request = $http({
			method: "get",
			url: "http://api.kme.si/v1/articles?resource_id=22&order=desc&limit=20",
			params: {
                  //time: timestamp
			}
		});
		return( request.then( handleSuccess, handleError ) );
	}
   

	function handleError(response) {

		if (response.status == "403")
		{
			//AuthenticationService.ClearCredentials();
			//$location.path('/login');
			//return;
		}
		
		if (!angular.isObject( response.data ) || !response.data.message) 
		{
			return( $q.reject( "An unknown error occurred." ) );
		}
		// Otherwise, use expected error message.
		return( $q.reject(response.data.message) );
	}

	function handleSuccess( response ) 
	{
		return( response.data );
	}
	
}]);


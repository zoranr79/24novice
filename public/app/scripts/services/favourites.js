'use strict';

/**
 * @ngdoc service
 * @name articlesApp.favourites
 * @description
 * # favourites
 * Service in the articlesApp.
 */
angular.module('articlesApp').service('favouritesService', ['helpers', function (helpers) {
    
	  	var service = {};
		
	  	service.getFavouritesData = getFavouritesData;
	  	service.removeFromFavourites = removeFromFavourites;

	  	return service;
	  	
		function getFavouritesData() 
		{
			return (helpers.getFavorites().then(function(response) {
				return response;
			}));
		}
		
		function removeFromFavourites(articleId)
		{
			return (helpers.removeFromFavourites(articleId).then(function(response) {
				return response;
			}));
			
		}
}]);
